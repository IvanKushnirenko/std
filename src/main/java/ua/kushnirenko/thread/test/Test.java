package ua.kushnirenko.thread.test;

public class Test {

    public static void main(String[] args) throws InterruptedException {
        A a = new A();
        B b = new B();

        Thread t = new Thread(new Executor2(a, b));
        t.start();
        Thread.sleep(5_001L);
    }


}

class A {
    public synchronized void a() {
        try {
            this.wait(5_000L);
        } catch (InterruptedException e) {
            System.out.println("Interrupted in A");
        }
        System.out.println("Release A.a");
        Thread.currentThread().interrupt();
    }
}

class B {
    public synchronized void b() {
        try {
            this.wait(1_000_000_000_000L);
        } catch (InterruptedException e) {
            System.out.println("Interrupted in B");
        }
    }

}

class Executor2 implements Runnable {

    A a;
    B b;

    public Executor2(A a, B b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void run() {
        a.a();
        System.out.println("start b");
        b.b();
    }
}