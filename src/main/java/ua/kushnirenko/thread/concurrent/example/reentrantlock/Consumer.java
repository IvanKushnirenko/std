package ua.kushnirenko.thread.concurrent.example.reentrantlock;

public class Consumer implements Runnable {

    private Store store;

    public Consumer(Store store) {
        this.store = store;
    }

    @Override
    public void run() {
        for (int i = 0; i < 6; i++) {
            store.get();
            System.out.println("Осталось купить: " + (6 - i - 1));
        }
    }
}
