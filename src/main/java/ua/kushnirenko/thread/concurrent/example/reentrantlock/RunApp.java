package ua.kushnirenko.thread.concurrent.example.reentrantlock;

public class RunApp {

    public static void main(String[] args) {
        Store s = new Store();

        Thread producer = new Thread(new Producer(s));
        Thread consumer = new Thread(new Consumer(s));

        consumer.start();
        producer.start();
    }

}
