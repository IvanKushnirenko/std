package ua.kushnirenko.thread.concurrent.example.reentrantlock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Store {

    ReentrantLock locker;
    Condition condition;

    private int amount;

    public Store() {
        locker = new ReentrantLock();
        condition = locker.newCondition();
        amount = 0;
    }

    public void get() {
        try {
            locker.lock();
            while (amount < 1) {
                condition.await();
            }
            amount--;
            System.out.println("Покупатель купил 1 товар");
            System.out.println("Товаров на складе: " + amount);

            condition.signal(); // signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            locker.unlock();
        }
    }

    public void put() {
        try {
            locker.lock();
            while (amount > 3) {
                condition.await();
            }
            amount++;
            System.out.println("Производитель добавил 1 товар");
            System.out.println("Товаров на складе: " + amount);

            condition.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            locker.unlock();
        }
    }
}
