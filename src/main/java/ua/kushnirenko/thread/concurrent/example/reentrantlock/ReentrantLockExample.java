package ua.kushnirenko.thread.concurrent.example.reentrantlock;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockExample {

    public static final DateFormat FORMAT = new SimpleDateFormat("HH:mm:ss");

    static class ValueContainer {

        private int x;

        public ValueContainer(int x) {
            this.x = x;
        }

        public int getX() {
            log("Текущее значение: " + x);
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }
    }

    static class ValueProcessor implements Runnable {

        ReentrantLock locker;
        ValueContainer container;

        public ValueProcessor(ReentrantLock locker, ValueContainer container) {
            this.locker = locker;
            this.container = container;
        }

        @Override
        public void run() {
            log("пытается захватить ReentrantLocker...");
            try {
                locker.lock();
                for (int i = 0; i < 5; i++) {
                    log("пытается увеличить значение на 1...");
                    container.setX(container.getX() + 1);
                }
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                locker.unlock();
            }
        }
    }

    private static void log(String msg) {
        System.out.println(FORMAT.format(new Date()) + " - " + Thread.currentThread().getName() + ": " + msg);
    }

    public static void main(String[] args) {
        ReentrantLock locker = new ReentrantLock();
        ValueContainer vc = new ValueContainer(0);

        for (int i = 1; i < 5; i++) {
            Thread t = new Thread(new ValueProcessor(locker, vc));
            t.setName("Поток " + i);
            t.start();
        }
    }
}
