package ua.kushnirenko.thread.concurrent.example.excharger;

import java.util.concurrent.Exchanger;


public class RunApp {

    public static void main(String[] args) {
        Exchanger<String> stringExchanger = new Exchanger<>();

        Thread t1 = new Thread(new MessageThread(stringExchanger, "Hello world!"));
        t1.setName("This thread had say: - Hello world! - But says: ");

        Thread t2 = new Thread(new MessageThread(stringExchanger, "I hate everyone!"));
        t2.setName("This thread must say: - I hate everyone - But says: ");

        t1.start();
        t2.start();
    }
}
