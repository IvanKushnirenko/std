package ua.kushnirenko.thread.concurrent.example.phaser;

import java.util.concurrent.Phaser;

import static java.lang.Thread.sleep;


public class PhaseThread implements Runnable {

    private Phaser phaser;

    public PhaseThread(Phaser phaser) {
        this.phaser = phaser;
        phaser.register();
    }


    @Override
    public void run() {
        RunApp.log("выполняет фазу " + phaser.getPhase());
        phaser.arriveAndAwaitAdvance();

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        RunApp.log("выполняет фазу " + phaser.getPhase());
        phaser.arriveAndAwaitAdvance();

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        RunApp.log("выполняет фазу " + phaser.getPhase());
        phaser.arriveAndDeregister();

    }
}


