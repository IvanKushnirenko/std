package ua.kushnirenko.thread.concurrent.example.phaser;

import java.util.concurrent.Phaser;

public class RunApp {

    public static void main(String[] args) {
        Thread.currentThread().setName("\tMain thread: ");

        Phaser phaser = new Phaser(1);

        Thread t1 = new Thread(new PhaseThread(phaser));
        t1.setName("Поток 1");
        t1.start();

        Thread t2 = new Thread(new PhaseThread(phaser));
        t2.setName("Поток 2");
        t2.start();

        int phase = phaser.getPhase();
        phaser.arriveAndAwaitAdvance();
        log("Фаза " + phase + " завершена");
        // ждем завершения фазы 1
        phase = phaser.getPhase();
        phaser.arriveAndAwaitAdvance();
        log("Фаза " + phase + " завершена");

        // ждем завершения фазы 2
        phase = phaser.getPhase();
        phaser.arriveAndAwaitAdvance();
        log("Фаза " + phase + " завершена");

        phaser.arriveAndDeregister();
    }

    public static void log(String msg) {
        System.out.println(Thread.currentThread().getName() + " " + msg);
    }

}
