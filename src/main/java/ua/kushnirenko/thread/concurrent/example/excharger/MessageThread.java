package ua.kushnirenko.thread.concurrent.example.excharger;

import java.util.concurrent.Exchanger;


public class MessageThread implements Runnable {

    private Exchanger<String> ex;
    private String msg;

    public MessageThread(Exchanger<String> exchanger, String msg) {
        this.ex = exchanger;
        this.msg = msg;
    }

    @Override
    public void run() {
        try {
            msg = ex.exchange(msg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " " + msg);
    }
}
