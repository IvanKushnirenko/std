package ua.kushnirenko.thread.concurrent.example.semaphore;

import java.util.concurrent.Semaphore;

import static ua.kushnirenko.thread.concurrent.example.semaphore.RunApp.log;

public class Table {

    private Semaphore semaphore;

    int thinkers;

    public Table() {
        semaphore = new Semaphore(2, false);
        thinkers = 0;
    }

    public void sitDown() {
        try {
            semaphore.acquire();
            log(" сел за стол.");
        } catch (InterruptedException e) {
            e.printStackTrace();
            semaphore.release();
        }
    }

    public void standUp() {
        log(" встал из-за стола.");
        semaphore.release();
    }
}
