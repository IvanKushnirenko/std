package ua.kushnirenko.thread.concurrent.example.semaphore;

public class RunApp {

    public static void log(String msg) {
        System.out.println(Thread.currentThread().getName() + msg);
    }

    public static void main(String[] args) {
        Table table = new Table();

        for (int i = 1; i < 5; i++) {
            Thread t = new Thread(new Philosopher(i, table));
            t.setName("Философ " + i);
            t.start();
        }
    }

}
