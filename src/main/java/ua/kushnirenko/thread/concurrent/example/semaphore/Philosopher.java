package ua.kushnirenko.thread.concurrent.example.semaphore;

import static ua.kushnirenko.thread.concurrent.example.semaphore.RunApp.log;

public class Philosopher implements Runnable {

    private Table t;

    public Philosopher(int i, Table t) {
        this.t = t;
    }

    @Override
    public void run() {
        log(" хочет сесть за стол.");
        t.sitDown();
        try {
            System.out.println("");
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t.standUp();
    }
}
