package ua.kushnirenko.thread.buffer.interrupt_buffer;

public class RunApp {

    public static void main(String[] args) throws InterruptedException {
        InterruptAtomicBuffer buff = new InterruptAtomicBuffer();

        for (int i = 1; i < 5; i++) {
            Thread t = new Thread(new Producer(buff));
            t.setName("producer-" + i);
            t.start();
            Thread.sleep(100);
        }

        System.out.println("debug line");

        for (int i = 1; i < 3; i++) {
            Thread t = new Thread(new Consumer(buff));
            t.setName("consumer-" + i);
            t.start();
            System.out.println("debug line-2");
        }
    }
}
