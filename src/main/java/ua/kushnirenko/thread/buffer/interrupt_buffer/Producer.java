package ua.kushnirenko.thread.buffer.interrupt_buffer;

import static ua.kushnirenko.thread.buffer.ordinary_buffer.RunApp.log;

public class Producer implements Runnable {

    private InterruptAtomicBuffer buff;

    public Producer(InterruptAtomicBuffer buff) {
        this.buff = buff;
    }

    @Override
    public void run() {
        for (int i = 0; i < 2; i++) {
            log("is going to PUT val " + i);
            buff.put(i);
            log(i + " produced");
            try {
                Thread.sleep(100L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        log("JOB IS DONE.");
    }
}
