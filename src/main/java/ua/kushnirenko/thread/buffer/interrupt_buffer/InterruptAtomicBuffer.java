package ua.kushnirenko.thread.buffer.interrupt_buffer;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;

public class InterruptAtomicBuffer {

    private Queue<AwaitingElement> waitProducers;
    private Queue<AwaitingElement> waitConsumers;

    private ReentrantLock locker;

    private Integer val;

    public InterruptAtomicBuffer() {
        waitProducers = new LinkedList<>();
        waitConsumers = new LinkedList<>();
        locker = new ReentrantLock();
    }

    public void put(Integer val) {
        locker.lock();
        while (this.val != null) {
            AwaitingElement ae = new AwaitingElement();
            waitProducers.offer(ae);
            locker.unlock();
            try {
                ae.goOff();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            locker.lock();
        }
        this.val = val;
        if (waitConsumers.size() != 0) {
            waitConsumers.poll().awake();
        }
        locker.unlock();
    }

    public Integer get() {
        locker.lock();
        while (this.val == null) {
            AwaitingElement ae = new AwaitingElement();
            waitConsumers.offer(ae);
            locker.unlock();
            try {
                ae.goOff();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            locker.lock();
        }
        int tmp = val;
        val = null;
        if (waitProducers.size() != 0) {
            locker.unlock();
            waitProducers.poll().awake();
        }
        locker.unlock();
        return tmp;
    }
}

