package ua.kushnirenko.thread.buffer.interrupt_buffer;

import static ua.kushnirenko.thread.buffer.ordinary_buffer.RunApp.log;

public class Consumer implements Runnable {

    private InterruptAtomicBuffer buff;

    public Consumer(InterruptAtomicBuffer buff) {
        this.buff = buff;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            log("is going to GET val.");
            int val = buff.get();
            log(val + " consumed.");
        }
        log("JOB IS DONE.");
    }
}
