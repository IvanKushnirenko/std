package ua.kushnirenko.thread.buffer.interrupt_buffer;

public class AwaitingElement {

    public synchronized void goOff() throws InterruptedException {
        this.wait();
    }

    public synchronized void awake() {
        this.notify();
        Thread.currentThread().interrupt();
    }
}
