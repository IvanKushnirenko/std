package ua.kushnirenko.thread.buffer.interrupt_buffer;

import java.util.LinkedList;
import java.util.Queue;

public class InterruptAtomicBuffer_2 {

    private Queue<Thread> waitProducers;
    private Queue<Thread> waitConsumers;

    private Integer val;

    public InterruptAtomicBuffer_2() {
        waitProducers = new LinkedList<>();
        waitConsumers = new LinkedList<>();
    }

    public synchronized void put(Integer val) {
        while (this.val != null) {

            try {
                waitProducers.offer(Thread.currentThread());
                wait();
            } catch (InterruptedException e) {
            }
        }
        this.val = val;
        if (waitConsumers.size() != 0) {
            waitConsumers.poll().interrupt();
        }
    }

    public synchronized Integer get() {
        while (this.val == null) {
            waitConsumers.offer(Thread.currentThread());
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        int tmp = val;
        val = null;
        if (waitProducers.size() != 0) {
            waitProducers.poll().interrupt();
        }
        return tmp;
    }

}

