package ua.kushnirenko.thread.buffer.ordinary_buffer;

/**
 * Есть вероятность, что Producer-ы никогда не будут помещены в WaitSet, т.е., работа Consumer-ов будет зациклена
 * в методе put(Integer val);
 */
public class AtomicBuffer_wrong_2 {

    private Integer val;

    public synchronized void put(Integer val) throws InterruptedException {
        while (this.val != null) {
            RunApp.log("moved to waitSet.");
            this.wait();
            if (val != null) {
                notify();
            }
        }
        RunApp.log("set value to: " + val);
        this.val = val;
        notify();
    }

    public synchronized Integer get() throws InterruptedException {
        while (val == null) {
            RunApp.log("moved to waitSet.");
            this.wait();
            if (val == null) {
                notify();
            }
        }
        RunApp.log("get value: " + val);
        int tmp = val;
        this.val = null;
        this.notify();
        return tmp;
    }
}
