package ua.kushnirenko.thread.buffer.ordinary_buffer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RunApp {

    public static final DateFormat df = new SimpleDateFormat("mm:ss.SSS - ");

    public static void main(String[] args) throws InterruptedException {
        AtomicBuffer_wrong_1 buff = new AtomicBuffer_wrong_1();

        for (int i = 1; i < 11; i++) {
            Thread t = new Thread(new Producer(buff, 10000));
            t.setName("producer-" + i);
            t.start();
        }

        for (int i = 1; i < 2; i++) {
            Thread t = new Thread(new Consumer(buff, 10000));
            t.setName("consumer-" + i);
            t.start();
        }
    }

    public static void log(String msg) {
        System.out.printf(df.format(new Date()) + Thread.currentThread().getName() + "::" + msg + "\n");
    }
}
