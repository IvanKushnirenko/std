package ua.kushnirenko.thread.buffer.ordinary_buffer;

public class Consumer implements Runnable {

    private AtomicBuffer_wrong_1 buff;
    private long rest;

    public Consumer(AtomicBuffer_wrong_1 buff, long rest) {
        this.buff = buff;
        this.rest = rest;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 10; i++) {
                RunApp.log("is going to get val.");
                int val = buff.get();
                RunApp.log("obtained val is: " + val);
//                Thread.currentThread().sleep(rest);
            }
            RunApp.log("finish it's work.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
