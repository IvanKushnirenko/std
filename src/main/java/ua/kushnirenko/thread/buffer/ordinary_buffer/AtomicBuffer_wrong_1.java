package ua.kushnirenko.thread.buffer.ordinary_buffer;

/**
 * Работать не будет, т.к., и производителей и потребителей может быть более 1.
 * Рассмотрим ситуацию, когда 4 производителя и 1 потребитель.
 * 1. Пришёл PROD, положил val, попал в WaitSet;
 * 2,3,4.Пришли PROD, все в WaitSet;
 * 5. Пришёл CONS, забрал, разбудил одного PROD-a
 * ------- в WaitSet - 3 PROD, 1 CONS;
 * ------- в BlockedSet - 1 PROD;
 * 6. Работает PROD из BlockedSet, и будит опять PROD.
 * 7. Всё спят.
 */
public class AtomicBuffer_wrong_1 {

    private Integer val;

    private int waitProducers = 0;
    private int waitConsumers = 0;

    public synchronized void put(Integer val) throws InterruptedException {
        while (this.val != null) {
            RunApp.log("moved to waitSet.");
            waitProducers++;
            this.wait();
            waitProducers--;
        }
        RunApp.log("set value to: " + val);
        this.val = val;
        if (waitConsumers > 0) {
            notify();
        }
    }

    public synchronized Integer get() throws InterruptedException {
        while (val == null) {
            RunApp.log("moved to waitSet.");
            waitConsumers--;
            this.wait();
            waitConsumers++;
        }
        RunApp.log("get value: " + val);
        int tmp = val;
        this.val = null;
        if (waitProducers > 0) {
            this.notifyAll();
        }
        return tmp;
    }
}
