package ua.kushnirenko.thread.buffer.ordinary_buffer;

import java.util.concurrent.TimeoutException;

public class AtomicBuffer_question {

    private Integer val;

    public synchronized void put(Integer val, long timeout) throws InterruptedException, TimeoutException {
        long waitTime = timeout;
        while (this.val != null && waitTime > 0) {
            RunApp.log("moved to waitSet.");
            long t1 = System.currentTimeMillis();
            wait(timeout);
            long t2 = System.currentTimeMillis();
            waitTime -= t1 - t2;
        }
        if (this.val != null) throw new TimeoutException();
        RunApp.log("set value to: " + val);
        this.val = val;
        notifyAll();
    }

    public synchronized Integer get(long timeout) throws InterruptedException, TimeoutException {
        long waitTime = timeout;
        while (val == null && waitTime > 0) {
            RunApp.log("moved to waitSet.");
            long t1 = System.currentTimeMillis();
            this.wait();
            long t2 = System.currentTimeMillis();
            waitTime -= t1 - t2;
        }
        if (val == null) throw new TimeoutException();
        RunApp.log("get value: " + val);
        int tmp = val;
        this.val = null;
        this.notifyAll();
        return tmp;
    }
}
