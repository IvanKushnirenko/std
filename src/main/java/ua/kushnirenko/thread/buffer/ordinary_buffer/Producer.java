package ua.kushnirenko.thread.buffer.ordinary_buffer;

public class Producer implements Runnable {

    private AtomicBuffer_wrong_1 buff;
    private long rest;

    public Producer(AtomicBuffer_wrong_1 buff, long rest) {
        this.buff = buff;
        this.rest = rest;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 6; i++) {
                RunApp.log("is going to put val " + i);
                buff.put(i);
                RunApp.log("put val successfully.");
//                Thread.currentThread().sleep(rest);
            }
            RunApp.log("finish it's work.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
