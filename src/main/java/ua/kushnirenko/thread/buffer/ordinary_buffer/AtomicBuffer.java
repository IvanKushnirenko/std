package ua.kushnirenko.thread.buffer.ordinary_buffer;

public class AtomicBuffer {

    private Integer val;

    public synchronized void put(Integer val) throws InterruptedException {
        while (this.val != null) {
            RunApp.log("moved to waitSet.");
            this.wait();
            Thread.currentThread().sleep(2000);
        }
        RunApp.log("set value to: " + val);
        this.val = val;
        notifyAll();
    }

    public synchronized Integer get() throws InterruptedException {
        while (val == null) {
            RunApp.log("moved to waitSet.");
            this.wait();
        }
        RunApp.log("get value: " + val);
        int tmp = val;
        this.val = null;
        Thread.currentThread().sleep(2000);
        this.notifyAll();
        return tmp;
    }
}
