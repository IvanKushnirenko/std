package ua.kushnirenko.thread.jenkow.semaphore;

import ua.kushnirenko.thread.jenkow.semaphore.examples.Receiver;
import ua.kushnirenko.thread.jenkow.semaphore.examples.Sender;

import static java.lang.Thread.sleep;

public class SimpleSemaphore {

    private boolean isSignaled = false;

    public synchronized void signalWaiter() {
        this.isSignaled = true;
        notify();
    }

    public synchronized void startWaitingForSignal() throws InterruptedException {
        while (!isSignaled) {
            wait();
        }
        isSignaled = false;
    }

    public static void main(String[] args) throws InterruptedException {
        SimpleSemaphore semaphore = new SimpleSemaphore();

        new Thread(new Receiver(semaphore)).start();

        System.out.println("Receicer is waiting...");
        sleep(3000l);
        System.out.println("New Sender is coming...");
        new Thread(new Sender(semaphore)).start();
    }
}
