package ua.kushnirenko.thread.jenkow.semaphore.examples;

import ua.kushnirenko.thread.jenkow.semaphore.SimpleSemaphore;

import static java.lang.Thread.sleep;

public class Sender implements Runnable {

    private SimpleSemaphore semaphore;

    int iter_cnt = 6;

    public Sender(SimpleSemaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        while (iter_cnt != 0) {
            try {
                sleep(2000L); //что-то долго делаю, когда закончил, нужно сообщить об этом другому потоку.
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
            System.out.println("Sender is doing smth...");
            semaphore.signalWaiter();
            iter_cnt--;
        }
    }
}
