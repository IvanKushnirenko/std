package ua.kushnirenko.thread.jenkow.semaphore.examples;

import ua.kushnirenko.thread.jenkow.semaphore.SimpleSemaphore;

import static java.lang.Thread.sleep;

public class Receiver implements Runnable {

    private SimpleSemaphore semaphore;

    private int inter_cnt = 4;

    public Receiver(SimpleSemaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        while (inter_cnt != 0) {
            try {
                semaphore.startWaitingForSignal(); // Для дальнейшей работы нужно сообщение о том, что я могу работать.
                //Будит меня Sender. Получаю сигнал, сбрасываю флаг семафора, начинаю что-то делать. В семафоре проверк а,
                // если флаг не выставлялся, то опять ложусь спать.
                System.out.println("Receiver: I am awakened and start doing something!");
                sleep(1000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
