package ua.kushnirenko.thread.jenkow.semaphore;

public class CountSemaphore {

    private int limit = 0;
    private int signal_cnt = 0;

    public CountSemaphore(int limit) {
        this.limit = limit;
    }

    //В таком случае первый, кто вызовет signal, заблокирует ресурс.
    public synchronized void signal() throws InterruptedException {
        while (signal_cnt == limit) {
            wait();
        }
        signal_cnt++;
        notify();
    }

    public synchronized void startWaiting() throws InterruptedException {
        if (signal_cnt == 0) {
            wait();
        }
        signal_cnt--;
        this.notify(); // Разбудить поток, который не может увеличить счётчик сигналов из-за достижения лимита
    }

    public static void main(String[] args) {

    }
}
