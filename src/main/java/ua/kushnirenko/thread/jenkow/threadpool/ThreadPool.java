package ua.kushnirenko.thread.jenkow.threadpool;

import ua.kushnirenko.thread.jenkow.queue.BlockingQueue;

import java.util.ArrayList;
import java.util.List;

public class ThreadPool {

    private BlockingQueue<Runnable> taskQueue;

    private List<PoolThread> threads = new ArrayList<>();

    private boolean isStopped;

    public ThreadPool(int minNoOffThreads, int maxNoOffThreads) {
        taskQueue = new BlockingQueue(maxNoOffThreads);

        for (int i = 0; i < minNoOffThreads; i++) {
            threads.add(new PoolThread(taskQueue));
        }
        for (PoolThread pt : threads) {
            pt.start();
        }
    }

    public synchronized void execute(Runnable task) throws Exception {
        if (isStopped) throw new IllegalStateException("ThreadPool is stopped.");

        this.taskQueue.enqueue(task);
    }

    public synchronized void stop() {
        isStopped = true;

        for (PoolThread pt : threads) {
            pt.doStop();
        }
    }
}
