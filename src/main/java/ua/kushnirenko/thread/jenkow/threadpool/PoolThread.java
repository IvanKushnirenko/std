package ua.kushnirenko.thread.jenkow.threadpool;

import ua.kushnirenko.thread.jenkow.queue.BlockingQueue;

public class PoolThread extends Thread {

    private BlockingQueue<Runnable> queue;

    private boolean isStopped = false;

    public boolean isStopped() {
        return isStopped;
    }

    public PoolThread(BlockingQueue<Runnable> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (!isStopped) {
            try {
                Runnable r = queue.dequeue();
                r.run();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void doStop() {
        isStopped = true;
        this.interrupt(); // Тут будятся потоки, которые попали в WaitSet при выполнении Runnable.
        // Брошенные исключения перехватываются в блоке catch (строка 25-27), после чего проверятся флаг isStopped,
        // программа выходит из метода run().
    }
}
