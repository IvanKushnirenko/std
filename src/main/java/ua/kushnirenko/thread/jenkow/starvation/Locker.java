package ua.kushnirenko.thread.jenkow.starvation;

/*
    Делегируем экземпляр локера и на нём синхронизируем потоки, которые нам нужны.
 */
public class Locker {

    private Thread invader;
    private boolean isLocked;

    public synchronized void lock() throws InterruptedException {
        /*
        Потоки заходят в синхронизированную секцию, первый зашедший выставляет флаг isLocked, после чего
        он освобождает монитор Locker-a, выйдя из метода lock(). След. поток заходит в этот метод, захватывает монитор,
        проверяет флаг isLocked = false и попадает в WaitSet(). Монитор освобождается. Т.е., пока блокировка не будет
        снята потоком, который её установил, то все потоки будут попадать в WaitSet-е.
         */
        while (isLocked) {
            wait();
        }
        invader = Thread.currentThread();
        isLocked = true;
    }

    public synchronized void unlock() {
        if (invader != Thread.currentThread()) {
            throw new IllegalMonitorStateException("This thread didn't call lock().");
        }
        invader = null;
        isLocked = false;
        notify();// notifyAll() вызывать нет смысла, всё равно "проснётся" только один поток.
        /*
        Проблема: notify() будит случайный поток, выбирая его из множества потоков, находящихся в waitSet-e. Т.е., есть
        вероятность, что некоторые потоки никогда не будут разбужены ОС для выполнения, т.е., будут starved to death.
        Получается, что множество потоков приличное количество времени находятся в WaitSet, а не в blockedSet на входе
        в synchronized метод lock().
        Решение: вызывать для каждго потока метод wait на одном конкретном обьекте, т.е., для каждого потока свой
        конкретый объект, на котором он ожидает. Тогда класс Locker сможет управлять пробуждением потоков, решая, какой
        именно в данный момент следует разбудить.
         */
    }
}
