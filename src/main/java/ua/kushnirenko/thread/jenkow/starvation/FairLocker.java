package ua.kushnirenko.thread.jenkow.starvation;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class FairLocker {

    private boolean isLocked = false;
    private Thread lockingThread = null;
    private List<WaitingMonitor> waitingThreads = new ArrayList<>();

    public void lock() throws InterruptedException {
        WaitingMonitor queueObject = new WaitingMonitor();  // Создаём без проблем, сколько потоков зашло, столько и создало
        boolean isLockedForThisThread = true;

        synchronized (this) {
            waitingThreads.add(queueObject); // добавляем не все сразу, а по очереди, т.к., важно сохранить очерёдность, в какой потоки потом будить.
            // очередь не потокобезопасна
        }

        while (isLockedForThisThread) { //1 раз заходим сюда в любом случае
            synchronized (this) {
                isLockedForThisThread = isLocked || waitingThreads.get(0) != queueObject; //Если не заблокирован и я первый в очереди,
                if (!isLockedForThisThread) { //isLockedForThisThread = false, то:
                    isLocked = true; // лок флаг,
                    waitingThreads.remove(queueObject); // уходим из очереди
                    lockingThread = Thread.currentThread(); // становимся в на провереку.
                    return; // выходим из метода
                }
            } // Если либо заблокирован, либо я не первый в очереди
            try {
                queueObject.doWait(); // пытаемся ждать,
            } catch (InterruptedException e) {
                synchronized (this) {
                    waitingThreads.remove(queueObject);// если чё случилось(кто-то попытался остановить), выходим из очереди.
                }
                throw e;
            }
        }
    }

    public synchronized void unlock() {
        if (this.lockingThread != Thread.currentThread()) {
            throw new IllegalMonitorStateException(
                    "Calling thread has not locked this lock");

        }
        isLocked = false;
        lockingThread = null;
        if (waitingThreads.size() > 0) {
            waitingThreads.get(0).doNotify();
        }
    }

    public static void main(String[] args) {
        NumberFormat FORMATTER = new DecimalFormat("#0.00#");
        BigDecimal bigDecimal = BigDecimal.valueOf((double) 96 / 100);
//        System.out.println(bigDecimal);
//        System.out.println(FORMATTER.format(bigDecimal.setScale(2, RoundingMode.HALF_DOWN).floatValue()));
        System.out.println();
    }

}
