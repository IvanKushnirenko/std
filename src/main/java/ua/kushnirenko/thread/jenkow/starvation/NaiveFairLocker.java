package ua.kushnirenko.thread.jenkow.starvation;

import java.util.ArrayList;
import java.util.List;

public class NaiveFairLocker {

    private boolean isLocked = false;
    private Thread lockingThread = null;
    private List<WaitingMonitor> waitingThreads = new ArrayList();

    private int locks = 0;

    public void lock() throws InterruptedException {
        WaitingMonitor queueObject = new WaitingMonitor();

        synchronized (this) {
            waitingThreads.add(queueObject);
        }

        while (isLocked || waitingThreads.get(0) != queueObject) {//либо заблокированно, либо я первый в очереди

            synchronized (queueObject) { // wait можно вызывать только из synchronized контекста.
                try {
                    queueObject.wait();
                } catch (InterruptedException e) {
                    waitingThreads.remove(queueObject);
                    throw e;
                }
            }
        }
        waitingThreads.remove(queueObject);
        isLocked = true;
        locks++;
        lockingThread = Thread.currentThread();
    }

    public synchronized void unlock() {
        if (this.lockingThread != Thread.currentThread()) {
            throw new IllegalMonitorStateException(
                    "Calling thread has not locked this lock");
        }
        locks--;
        if (locks == 0) {
            isLocked = false;
            lockingThread = null;
            if (waitingThreads.size() > 0) {
                WaitingMonitor queueObject = waitingThreads.get(0);
                synchronized (queueObject) {
                    queueObject.notify();
                }
            }
        }
    }
}
