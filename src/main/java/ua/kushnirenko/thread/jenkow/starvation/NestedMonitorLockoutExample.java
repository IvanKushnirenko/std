package ua.kushnirenko.thread.jenkow.starvation;

public class NestedMonitorLockoutExample {

    protected Object monitorObject = new Object();
    protected boolean isLocked = false;

    public void lock() throws InterruptedException {
        synchronized (this) {
            while (isLocked) {      /*
                                     Если зашли сюда, то получается, что держим монитор по this, а monitorObject
                                     is locked. Соотв., this залочен, и мы не можем выйти из бесконечного цикла,
                                     и сами  находимся в blockedSet. Далее, поток, захвативший monitorObject,
                                     не может выполнить unlock(), т.к., другой поток захватил монитор у this.
                                     */
                synchronized (this.monitorObject) {
                    this.monitorObject.wait();
                }
            }
            isLocked = true;
        }
    }
    /*
       Проблему можно решить просто - вызвать unlock(), что бы выйти из lock(). Но так ещё никому не удавалось)
       Получаемся заблокированными на неопределённое время.
     */
    public void unlock() {
        synchronized (this) {
            this.isLocked = false;
            synchronized (this.monitorObject) {
                this.monitorObject.notify();
            }
        }
    }


}
