package ua.kushnirenko.thread.jenkow;


public class ThreadLocalExample {

    public static void main(String[] args) {
        Localizator l = new Localizator();

        Thread t1 = new Thread(l);
        t1.setName("Поток 1");

        Thread t2 = new Thread(l);
        t2.setName("Поток 2");

        t1.start();
        t2.start();
    }
}

class Localizator implements Runnable {

    public static ThreadLocal<String> tl = new ThreadLocal<>();

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " sets value.");

        tl.set("first val by: " + Thread.currentThread().getName());

        try {
            Thread.currentThread().sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName() + " gets value: " + tl.get());
    }
}
