package ua.kushnirenko.thread.jenkow.javautilconcurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorServiceExample {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
//        ExecutorService executorService = Executors.newSingleThreadExecutor();
//        ExecutorService executorService = Executors.newScheduledThreadPool(10);

        /*
        Имплементации ExecutorService-a ThreadPoolExecutor и ScheduledThreadPoolExecutor
         */


/* void execute (Runnable r)
        executorService.execute(() -> {
            System.out.println("Asynchronous task is running...");          // тут код выполняется параллельно
        });
*/


/* Future submit(Runnable r)

        Future result = executorService.submit(() -> {
            System.out.println(Thread.currentThread().getName() + " start executing...");
            try {
                Thread.currentThread().sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " stop executing...");
        });
        System.out.println("Is job done: " + result.isDone());
        System.out.println("Result: " + result.get()); // поток блокируется, ждём результат.
*/


/* Future submit(Callable c)

        Future future = executorService.submit(() -> {
            System.out.println("Asynchronous Callable is working...");
            Thread.currentThread().sleep(3000L);
            return "Callable Result";
        });
        System.out.println("Waiting for results of Callable execution...");
        System.out.println("future.get() = " + future.get());

 */

/* Future invokeAny(Collection<& extendsCallable<T>> collection)

        Set<Callable<String>> callables = new HashSet<Callable<String>>();

        callables.add(() -> {
            return "Task 3";
        });

        callables.add(() -> {
            return "Task 1";
        });

        callables.add(() -> {
            return "Task 2";
        });

        String result = executorService.invokeAny(callables);
        System.out.println("result = " + result);
*/


        executorService.shutdown(); //
    }


}
