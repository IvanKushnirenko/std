package ua.kushnirenko.thread.jenkow;

public class RaceConditionExample {

        public volatile int val = 0;
//    public int val = 0;

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public void incrementation() {
        val += 1;
        try {
            Thread.currentThread().sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(new Racer());
        t1.setName("Поток 1");
        Thread t2 = new Thread(new Racer());
        t2.setName("Поток 2");

        t1.start();
        try {
            Thread.currentThread().sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t2.start();
    }
}

class Racer implements Runnable {

    RaceConditionExample rce = new RaceConditionExample();

    @Override
    public void run() {
        while (true) {
            rce.incrementation();
            System.out.println(Thread.currentThread().getName() + ": " + rce.getVal());
        }
    }
}
