package ua.kushnirenko.thread.jenkow.lock;

/**
 * Локер, который можно захватиывать несколько раз одним потоком.
 * Количесто вызовов метода lock и метода unlock() должно совпадать, иначе - блокировка снята не будет.
 */
public class ReentrantLock {

    private Thread invader;
    private int cnt = 0;
    private boolean isLocked = false;

    public synchronized void lock() throws InterruptedException {
        while (isLocked) {
            if (Thread.currentThread() == invader) {
                cnt++;
                return;
            }
            wait();
        }
        isLocked = true;
        invader = Thread.currentThread();
        cnt++;
    }

    public synchronized void unlock() throws InterruptedException {
        if (Thread.currentThread() != invader) throw new IllegalMonitorStateException();
        else {
            cnt--;
            if (cnt == 0) {
                invader = null;
                isLocked = false;
                notify();
            }
        }
    }
}
