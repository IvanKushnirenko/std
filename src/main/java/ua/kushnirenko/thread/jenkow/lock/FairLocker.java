package ua.kushnirenko.thread.jenkow.lock;

import java.util.Queue;


public class FairLocker {

    private Thread invader;
    private Queue<QueueElem> waitingThreads;
    private boolean isLocked = false;


    public void lock() throws InterruptedException {
        boolean isLockedForThisThread = true;
        QueueElem elem = new QueueElem();

        synchronized (this) {
            waitingThreads.offer(elem);
        }

        while (isLockedForThisThread) {
            synchronized (this) {
                isLockedForThisThread = isLocked || waitingThreads.peek() != elem;
                // первый выполняющий = false || false
                // второй выполняющий = true || false, т.е., заблокированно, но я первый в очереди
                // третий выполняющий = true || true , т.е., заблокированно, я второй в очереди
                if (!isLockedForThisThread) { // т.е., если не заблокированно и я первый в очереди
                    isLocked = true;
                    waitingThreads.poll();
                    invader = Thread.currentThread();
                    return;
                }
                // заблокированно, позиция в очереди не важна. Освобождаю монитор Locker-a.
                // В этот момент МОЖЕТ быть вызван метод unlock(), но уже не важно, я попадаю в waitSet.
            }
            try {
                elem.doWait();
            } catch (InterruptedException e) {
                synchronized (this) {
                    waitingThreads.peek();
                    throw e;
                }
            }
        }
    }

    public synchronized void unLock() {

    }

}

class QueueElem {

    private boolean isNotified = false;

    public synchronized void doWait() throws InterruptedException {
        while (!isNotified) {
            wait();
        }
        isNotified = false;
    }

    public synchronized void doNotify() {
        isNotified = true;
        notify();
    }
}
