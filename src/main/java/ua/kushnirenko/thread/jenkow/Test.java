package ua.kushnirenko.thread.jenkow;

public class Test {

    public static void main(String[] args) throws InterruptedException {
        ValWrap valWrap = new ValWrap();

        Thread t1 = new Thread(new Processor(valWrap));
        t1.setName("Поток 1");
        Thread t2 = new Thread(new Processor(valWrap));
        t2.setName("Поток 2");

        t1.start();
        Thread.currentThread().sleep(2000);
        t2.start();
    }

    static class ValWrap {

        int val = 1;

        void setVal(int val) {
            try {
                synchronized (this) {
                    System.out.println(Thread.currentThread().getName() + " is setting the value.");
                    this.val = val;
                    Thread.currentThread().sleep(6000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        int getVal() {
            synchronized (this) {
                return val;
            }
        }
    }

}

class Processor implements Runnable {

    Test.ValWrap valWrap;

    public Processor(Test.ValWrap valWrap) {
        this.valWrap = valWrap;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " tries to set up Val...");
        valWrap.setVal(valWrap.getVal() + 1);
        System.out.println("Value is set up. Releasing monitor...");
    }
}