package ua.kushnirenko.thread.jenkow.read_write_lock;

import ua.kushnirenko.thread.jenkow.starvation.WaitingMonitor;

import java.util.*;

/*
Locker для чтения/записи с очередью на запись и c reentrance.
 */
public class ReentrantReadWriteLocker {

    private ReadExpectantMonitor readersMonitor;    // Объект для WaitSet Reader-ов.
    private Map<Thread, Integer> read_map;                   // "Читающие" читатели

    private Queue<WaitingMonitor> writers;            // Очередь на запись
    private Thread writeInvader = null;             // В т.ч. и маркер блокировки на запись.

    private int writeLocks_cnt = 0;                 // счётчик блокировки одним Writer-ом.

    public ReentrantReadWriteLocker() {
        this.readersMonitor = new ReadExpectantMonitor();
        this.writers = new LinkedList<>();
        read_map = new HashMap<>();
    }

    /*
    Читать нельзя тогда, когда кто-то пишет, либо собирается писать.
    Кто заблокировал - важно, т.к., если это Writer, то читать нельзя.
    */
    public void readLock() {
        Thread currThread = Thread.currentThread();

        synchronized (this) {
            // Если доступ для чтения запрашивает текуший Writer
            if (writeInvader != null && currThread == writeInvader) {
                read_map.put(currThread, 1);
                return;
            }

            // Если Reader уже имеет доступ, то увеличивается счётчик блокирований ресурса этим Reader-ом.
            Integer cnt = read_map.get(currThread);
            if (cnt != null) {
                read_map.replace(currThread, cnt++);
                return;
            }
        }

        boolean isAllowedToBlock = false;

        while (isAllowedToBlock == false) {
            synchronized (this) {
                //Если (никто не пишет) И (очередь на запись пустая):
                isAllowedToBlock = (writeInvader == null) && (writers.size() == 0);

                if (isAllowedToBlock == true) {
                    read_map.put(currThread, 1);
                    return;
                }
            }
            try {
                readersMonitor.doWait();
            } catch (InterruptedException e) {
                //Не делаем ничего, т.к. в read_map Reader не попал.
            }
        }
    }

    /*
    Писать нельзя тогда, когда кто-то читает или пишет.
    Кто заблокировал, НЕВАЖНО.
    */
    public void writeLock() {
        Thread currThd = Thread.currentThread();
        WaitingMonitor waitElem = new WaitingMonitor();

        synchronized (this) {
            //Если Reader уже читает, и (он единственный Reader) то
            if (read_map.containsKey(currThd) && read_map.size() == 1) {
                writeInvader = currThd;
                writeLocks_cnt++;
                return;
            }

            // Если Writer уже пишет, то увеличиваем счётчик его захватов
            if (writeInvader != null && writeInvader == currThd) {
                writeLocks_cnt++;
                return;
            }
            writers.offer(waitElem);
        }

        boolean isAllowedToBlock = false;
        while (isAllowedToBlock == false) {
            synchronized (this) {
                //Если (никто в данный момент не читает) И (никто в данный момент не пишет) И (я первый в очереди на запись):
                isAllowedToBlock = read_map.size() == 0 && writeInvader == null && writers.element() == waitElem;

                if (isAllowedToBlock) {
                    writeInvader = currThd;
                    writeLocks_cnt++;
                    writers.poll();
                    return;
                }
            }
            try {
                waitElem.doWait();
            } catch (InterruptedException e) {
                synchronized (this) {
                    writers.remove(waitElem);
                }
            }
        }
    }

    /*
    Тут всё просто, когда Reader дочитал, его нужно удалить из read set;
     */
    public void readUnlock() {
        Thread currThd = Thread.currentThread();

        synchronized (this) {
            Integer cnt = read_map.get(currThd);
            if (cnt == null) throw new IllegalMonitorStateException("Этот поток не блокировал ресурс для чтения.");
            if (cnt == 1) read_map.remove(currThd);
            else read_map.replace(currThd, cnt--); // cnt > 1
        }
    }

    public void writeUnlock() {
        Thread currThd = Thread.currentThread();

        synchronized (this) {
            if (currThd != writeInvader)
                throw new IllegalMonitorStateException("Этот поток не блокировал ресурс для записи.");
            if (writeLocks_cnt > 1) {
                writeLocks_cnt--;
                return;
            }
            // Когда writeLocks_cnt == 1
            writeInvader = null;
            writeLocks_cnt--; //  Можно и writeLocks_cnt = 0;
            if (writers.size() > 0) {
                writers.element().doNotify();// Если есть Writer-ы, будим первого в очереди.
            } else readersMonitor.doNotify(); // Иначе будим сразу всех Reader-ов, если они есть.
        }
    }
}
