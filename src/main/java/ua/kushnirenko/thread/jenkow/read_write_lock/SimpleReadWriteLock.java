package ua.kushnirenko.thread.jenkow.read_write_lock;

public class SimpleReadWriteLock {

    private int readers = 0;
    private int waitWriters = 0;

    private Thread writeInvader;

    public synchronized void readLock() throws InterruptedException {
        while (writeInvader != null || waitWriters > 0) {
            wait();
        }
        readers++;
    }

    public synchronized void writeLock() throws InterruptedException {
        while (writeInvader != null || readers != 0) {
            waitWriters++;
            wait();
        }
        writeInvader = Thread.currentThread();
        waitWriters--;
    }

    public synchronized void readUnlock() {
        readers--;
        notifyAll();
    }

    public synchronized void writeUnlock() {
        writeInvader = null;
        notifyAll();
    }
}
