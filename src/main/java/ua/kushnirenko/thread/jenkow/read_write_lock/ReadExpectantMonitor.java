package ua.kushnirenko.thread.jenkow.read_write_lock;

public class ReadExpectantMonitor {

    private int expectants = 0;

    private boolean isNotified = false;

    public synchronized void doWait() throws InterruptedException {
        if (!isNotified) {
            expectants++;
            wait();
        }
        isNotified = false;
    }

    public synchronized void doNotify() {
        if (expectants > 0) {
            isNotified = true;
            notifyAll(); // все ожидающие уйдут в BlockedSet.
        }
    }
}
