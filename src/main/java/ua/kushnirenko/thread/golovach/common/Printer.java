package ua.kushnirenko.thread.golovach.common;

public class Printer implements Runnable {

    private String msg;
    private long timeout;

    public Printer(String msg, long timeout) {
        this.msg = msg;
        this.timeout = timeout;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println(msg);
            try {
                Thread.currentThread().sleep(timeout);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
