package ua.kushnirenko.thread.golovach.lab1;

public class Lab1 {

    public static void main(String[] args) throws InterruptedException {
        Thread coordinator = new Thread(new Coordinator());
        coordinator.start();
    }
}
