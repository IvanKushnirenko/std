package ua.kushnirenko.thread.golovach.lab1;

import ua.kushnirenko.thread.golovach.common.Printer;

public class Coordinator implements Runnable {


    @Override
    public void run() {
        try {
            for (int i = 0; i < 3; i++) {
                Runnable r1 = new Printer("A  .", 100l);
                Thread printerA = new Thread(r1);
                printerA.start();

                Runnable r2 = new Printer(".  B", 100l);
                Thread printerB = new Thread(r2);
                printerB.start();

                printerA.join();
                printerB.join();

                Runnable r3 = new ExtendedPrinter("  C", 10, printerA, printerB);
                Thread printerC = new Thread(r3);
                printerC.start();
                printerC.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
