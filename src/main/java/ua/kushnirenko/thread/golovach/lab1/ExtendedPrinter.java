package ua.kushnirenko.thread.golovach.lab1;

import ua.kushnirenko.thread.golovach.common.Printer;

public class ExtendedPrinter extends Printer {

    private Thread a;
    private Thread b;

    public ExtendedPrinter(String msg, long timeout, Thread a, Thread b) {
        super(msg, timeout);
        this.a = a;
        this.b = b;
    }

    @Override
    public void run() {
        try {
            a.join();
            b.join();
            System.out.println("-----");
            for (int i = 0; i < 6; i++) {
                System.out.println(" C ");
                Thread.currentThread().sleep(100l);
            }
            System.out.println("FINISH");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
