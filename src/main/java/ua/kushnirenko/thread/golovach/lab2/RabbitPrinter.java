package ua.kushnirenko.thread.golovach.lab2;

import org.jetbrains.annotations.NotNull;

public class RabbitPrinter implements Runnable {

    static int cnt = 0;

    public RabbitPrinter() {
        synchronized (RabbitPrinter.class) {
            cnt++;
        }
    }

    @Override
    public void run() {
        new Thread(new RabbitPrinter()).start();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(new RabbitPrinter()).start();
        for (int i = 0; i < 5; i++) {
            System.out.println(spaces(cnt) + cnt);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    @NotNull
    private static String spaces(int cnt) {
        StringBuilder emptyString = new StringBuilder();
        for (int i = 0; i < cnt; i++) {
            emptyString.append(" ");
        }
        return emptyString.toString();
    }
}
