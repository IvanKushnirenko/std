package ua.kushnirenko.thread.producer_consumer;

public class Store {

    private int amount = 0;

    private boolean isProductionEnded = false;

    public synchronized void get() throws InterruptedException {
        while (amount < 1) {
            if (isProductionEnded) {
                System.out.println(" Товаров в магазине больше не будет!");
                Thread.currentThread().interrupt();
                return;
            }
            wait();
        }
        amount--;
        System.out.println("Покупатель купил 1 товар");
        System.out.println("Товаров на складе: " + amount);
        notify();
    }

    public synchronized void put() {
        while (amount > 3) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        amount++;
        System.out.println("Производитель добавил 1 товар");
        System.out.println("Товаров на складе: " + amount);
        notify();
    }

    public synchronized void setProductionEnded(Boolean b) {
        System.out.println("Остановка производства...");
        this.isProductionEnded = b;
        notify();
    }
}
