package ua.kushnirenko.thread.producer_consumer;

public class Consumer implements Runnable {

    private Store store;

    public Consumer(Store store) {
        this.store = store;
    }

    @Override
    public void run() {
        for (int i = 0; i < 6; i++) {
            try {
                store.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Осталось купить: " + (6 - i - 1));
        }
    }
}
