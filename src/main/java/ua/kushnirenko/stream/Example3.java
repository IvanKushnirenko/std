package ua.kushnirenko.stream;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Примеры конвеерных методов stream-а
 */
public class Example3 {

    public static void main(String[] args) {
        //Конвеерные методы:
        Collection<String> collection = Arrays.asList("a1", "a2", "a3", "a4", "a1", "a5");
        Stream<String> stream = collection.stream();

        stream.filter(s -> s.equals("a1")).count();

        stream.skip(2).findFirst().orElse("a1");

        stream.distinct().collect(Collectors.toList());

        stream.map(s -> s + "_1").collect(Collectors.toList());

        stream.peek((e) -> System.out.print("," + e)).collect(Collectors.toList());

        stream.limit(2).collect(Collectors.toList());

        stream.sorted().collect(Collectors.toList());

        stream.mapToInt((s) -> Integer.parseInt(s)).toArray();
        //(stream.mapToDouble, stream.mapToLong)

        stream.flatMap((p) -> Arrays.asList(p.split(",")).stream()).toArray(String[]::new);


    }

}
