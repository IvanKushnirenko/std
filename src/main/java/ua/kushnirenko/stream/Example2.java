package ua.kushnirenko.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Примеры создания sream-ов
 */
public class Example2 {

    public static void main(String[] args) throws IOException {

        //1. Создание из коллекции:
        Collection<String> collection = Arrays.asList("a1", "a2", "a3", "a4");
        Stream<String> streamFromCollection = collection.stream();

        //2. Из значений:
        Stream<String> streamOfValues = Stream.of("a1", "a2", "a3", "a4");

        //3. Из массива:
        String[] arr = {"a1", "a2", "a3", "a4"};
        Stream<String> streamFromArray = Arrays.stream(arr);

        //4. Создание стрима из файла:
        Stream<String> streamFileLines = Files.lines(Paths.get("file.txt"));

        //5. Создание из строки:
        IntStream streamFromString = "123456789".chars(); //Возвращает ASCII код

        //6. Создание c помощью Stream.builder():
        Stream stream = Stream.builder().add(1).add(2).add(3).build();

        //7. Создание паралелльного стрима:
        Stream parallelStream = collection.parallelStream();

        //8. Создание бесконечного стрима с помощью iterate():
        Stream<Integer> streamFromIterate = Stream.iterate(1, n -> n + 1);

        //9. Создание бесконечного стрима с помощью generate():
        Stream<String> streamFromGenerate = Stream.generate(() -> "a1");
    }


}
