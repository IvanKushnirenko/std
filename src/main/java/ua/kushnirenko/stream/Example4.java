package ua.kushnirenko.stream;


import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Примеры терминальных методов stream-а
 */
public class Example4 {

    public static void main(String[] args) {
        Collection<Integer> collection = Arrays.asList(1, 3, 3, 5, 6, 7, 3, 0, 8, 9);
        Stream<Integer> stream = collection.stream();

        stream.findFirst().orElse(1);

        stream.findAny().orElse(1);

        stream.filter((s) -> s.equals(1)).collect(Collectors.toList());

        stream.filter(new Integer(5)::equals).count();

        stream.anyMatch(new Integer(6)::equals);

        stream.noneMatch(new Integer(10)::equals);

        stream.allMatch(new Integer(1)::equals);

        stream.min(Integer::compareTo).get();

        stream.max(Integer::compareTo).get();

        stream.forEach((i) -> i.toString());

        stream.forEachOrdered((i) -> i = i + 5);

        stream.map(Integer::highestOneBit).toArray(String[]::new);

        stream.reduce((i1, i2) -> i1 + i2).orElse(0);


    }
}
