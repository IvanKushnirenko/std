package ua.kushnirenko.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Example1 {

    public static void main(String[] args) {

        List<Integer> collection = new ArrayList<>();
        Random r = new Random();

        for (int i = 0; i < 20; i++) {
            collection.add(r.nextInt(20));
        }

        System.out.println(collection);
        System.out.println("Total count: " + collection.stream().count());
        System.out.println("Amount of elements that are greater than 10: "
                + collection.stream().filter(i -> i > 10).count());
        collection.stream().filter(i -> i > 10).forEach(i -> System.out.print(i + " "));
    }
}
