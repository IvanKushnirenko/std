package ua.kushnirenko.lambda.domain;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Person {

    public enum Sex {
        MALE, FEMALE
    }

    public String name;
    public LocalDate birthday;
    public Sex gender;
    public String email;

    public Person() {
    }

    public Person(String name, LocalDate birthday, Sex gender, String email) {
        this.name = name;
        this.birthday = birthday;
        this.gender = gender;
        this.email = email;
    }

    public int getAge() {
        return (int) birthday.until(LocalDate.now(), ChronoUnit.YEARS);
    }

    public void printPerson() {
        System.out.println("Person{" +
                "name='" + name + '\'' +
                ", birthday=" + birthday +
                ", gender=" + gender +
                ", email='" + email + '\'' +
                '}');
    }

    public static void main(String[] args) {
        Person person = new Person();
        person.name = "Ivan";
        person.birthday = LocalDate.of(1993, 10, 21);
        person.gender = Sex.MALE;
        person.email = "kushnirenkoivan@gmail.com";

        System.out.println(person.getAge());
    }

}
