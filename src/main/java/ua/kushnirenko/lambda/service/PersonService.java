package ua.kushnirenko.lambda.service;

import ua.kushnirenko.lambda.criteria.PersonCriteria;
import ua.kushnirenko.lambda.criteria.impl.CheckPersonEligibleForSelectiveService;
import ua.kushnirenko.lambda.domain.Person;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class PersonService {

    public static void printPersonsOlderThan(List<Person> roster, int age) {
        for (Person p : roster) {
            if (p.getAge() >= age) {
                p.printPerson();
            }
        }
    }

    public static void printPersonsWithinAgeRange(List<Person> roster, int low, int high) {
        for (Person p : roster) {
            if (low <= p.getAge() && p.getAge() < high) {
                p.printPerson();
            }
        }
    }

    public static void printPersons(List<Person> roster, PersonCriteria pc) {
        for (Person p : roster) {
            if (pc.isEligible(p)) {
                p.printPerson();
            }
        }
    }

    public static void printPersonsWithPredicate(List<Person> roster, Predicate<Person> tester) {
        for (Person p : roster) {
            if (tester.test(p)) {
                p.printPerson();
            }
        }
    }

    public static void processPersons(List<Person> roster, Predicate<Person> tester, Consumer<Person> block) {
        for (Person p : roster) {
            if (tester.test(p)) {
                block.accept(p);
            }
        }
    }

    public static void processPersonsWithFunction(
            List<Person> roster,
            Predicate<Person> tester,
            Function<Person, String> mapper,
            Consumer<String> block) {
        for (Person p : roster) {
            if (tester.test(p)) {
                String data = mapper.apply(p);
                block.accept(data);
            }
        }
    }

    public static <X, Y> void processElements(
            Iterable<X> source,
            Predicate<X> tester,
            Function<X, Y> mapper,
            Consumer<Y> block) {
        for (X p : source) {
            if (tester.test(p)) {
                Y data = mapper.apply(p);
                block.accept(data);
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Start printing persons that are eligible for certain criteria.");

        List<Person> roster = new ArrayList<>();
        roster.add(new Person("John Doe", LocalDate.of(1968, 7, 15), Person.Sex.MALE, "johnDoe@gmail.com"));
        roster.add(new Person("Kate Mitchel", LocalDate.of(1994, 5, 29), Person.Sex.FEMALE, "kate111@gmail.com"));
        roster.add(new Person("Artur Abraham", LocalDate.of(1973, 2, 11), Person.Sex.MALE, "abraham12@gmail.com"));
        roster.add(new Person("Ivan Ivanovich", LocalDate.of(1993, 10, 21), Person.Sex.MALE, "2137129@gmail.com"));

        //Create realization of specified criteria:
        printPersons(roster, new CheckPersonEligibleForSelectiveService());

        //Create anonymous class:
        printPersons(
                roster,
                new PersonCriteria() {
                    @Override
                    public boolean isEligible(Person p) {
                        return p.gender == Person.Sex.MALE
                                && p.getAge() >= 18
                                && p.getAge() <= 25;
                    }
                }
        );

        //Specify criteria by using lambda expression:
        printPersons(roster, p -> p.gender == Person.Sex.MALE && p.getAge() >= 18 && p.getAge() <= 25);

        //Using the interface java.util.function.Predicate<T>:
        printPersonsWithPredicate(roster, p -> p.gender == Person.Sex.MALE && p.getAge() >= 18 && p.getAge() <= 25);

        processPersons(roster,
                p -> p.gender == Person.Sex.FEMALE && p.getAge() >= 18 && p.getAge() <= 25,
                p -> p.printPerson());

        processPersonsWithFunction(
                roster,
                p -> p.gender == Person.Sex.MALE
                        && p.getAge() >= 18
                        && p.getAge() <= 25,
                p -> p.email,
                email -> System.out.println(email)
        );

        processElements(
                roster,
                p -> p.gender == Person.Sex.MALE
                        && p.getAge() >= 18
                        && p.getAge() <= 25,
                p -> p.email,
                email -> System.out.println(email)
        );

        roster.stream()
                .filter(
                        p -> p.gender == Person.Sex.MALE
                                && p.getAge() >= 18
                                && p.getAge() <= 25)
                .map(p -> p.email)
                .forEach(email -> System.out.println(email));
    }
}
