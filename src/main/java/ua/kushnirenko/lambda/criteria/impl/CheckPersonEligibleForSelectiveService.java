package ua.kushnirenko.lambda.criteria.impl;

import ua.kushnirenko.lambda.criteria.PersonCriteria;
import ua.kushnirenko.lambda.domain.Person;

public class CheckPersonEligibleForSelectiveService implements PersonCriteria {

    @Override
    public boolean isEligible(Person p) {
        return p.gender == Person.Sex.MALE &&
                p.getAge() >= 18 &&
                p.getAge() <= 25;
    }

}
