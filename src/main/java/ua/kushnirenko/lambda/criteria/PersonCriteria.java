package ua.kushnirenko.lambda.criteria;

import ua.kushnirenko.lambda.domain.Person;

@FunctionalInterface
public interface PersonCriteria {

    boolean isEligible(Person p);

}
